/**
* JetBrains Space Automation
* This Kotlin-script file lets you automate build activities
* For more info, see https://www.jetbrains.com/help/space/automation.html
*/

job("enviroment var checker") {
    container("ubuntu:latest") {
        shellScript {
            interpreter = "/bin/bash"
            location = "./env-var-check.sh"
        }
    }
}
