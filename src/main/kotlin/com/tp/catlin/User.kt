package com.tp.catlin

import java.util.Date

public var date = Date()
public class User<intstr>(val id: String, var name: String = "unknown", var email: String, var tel: intstr? = null) {
    fun greet (how: String = "Hello"): Unit  {
        println("${how} ${this.name}")
    }
    fun call (act: String = "open"): Unit{
        var callStatus: String = "close"
        if (act == "open" && callStatus != "open") {
            println("Calling ${this.tel} ...")
            callStatus = "opening"
            println("Connected.")
            callStatus = "open"
            println("[INFO] connected on ${date}")
        } else if (act == "close" && callStatus != "close") {
            println("Cutting call with ${this.tel} ...")
            callStatus = "closing"
            println("Disconnected")
            callStatus = "close"
            println("[INFO] disconnected on ${date}")
        } else if (act == "open" && (callStatus == "open" || callStatus == "opening")) {
            println("you are already on a call with this person or the call is connecting")
        } else if (act == "close" && (callStatus == "close" || callStatus == "closing")) {
            println("you're not on a call with this person anymore or the call is disconnecting")
        } else {
            println("error")
        }
    }
}
